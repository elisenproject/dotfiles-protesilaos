# My Emacs and other configurations for GNU/Linux

This is the set of files that powers my day-to-day computing experience.
GNU Emacs is its centrepiece, with other programs providing ancillary
functionalities.  In some cases, such as with the configurations for Vim
and Xterm, all relevant files are carried over from my pre-Emacs days
and are maintained as a contingency plan or in case I need to revisit
some old setup.

## How to reproduce my Emacs setup

This is discussed in the heading _"How to reproduce your dotemacs?"_ of
my `prot-emacs.org`.  Either find it there, or read it on the website:
<https://protesilaos.com/dotemacs/#h:0675f798-e2d9-4762-9df2-f47cd24cf00a>.

It also explains how to manage my dotfiles with the help of GNU Stow.

## Do not track my dotfiles (rolling, unstable, and untested)

_Ceci n'est pas une distribution Emacs._

This repo functions as a laboratory of experimentation for my computing
environment.  What I do with Emacs or any other programs in the
GNU/Linux milieu that form part of my dotfiles is only meant to work for
me.  As such, I offer no support whatsoever to those tracking this
repository and may introduce breaking changes without prior notice.

This is all to say that **you understand the risks associated with
tracking an ever-changing project that does not enjoy widespread testing
and whose target audience is only me**.  If you are fine with that and
are willing to assume responsibility for any possible breakage, then
please feel welcome to follow along.  You can always open an issue here
or contribute any fixes, if you will.

## BSPWM configuration

As of 2021-08-27 I am back to using BSPWM.  Emacs remains the
centrepiece of my computing environment, though I got bored with Xfce
and thought I would re-use my old configurations.  There have been some
updates and more will follow.  Everything should be considered a
work-in-progress until further notice.

## Copying

Unless otherwise noted, all code herein is distributed under the terms
of the GNU General Public License Version 3 or later.
